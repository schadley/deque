CXX = g++
CXXFLAGS = -Wall -Werror -pedantic --std=c++11 -g

all: test

test: Deque_tests.exe
	./Deque_tests.exe

Deque_tests.exe: Deque_tests.o Deque.o
	$(CXX) $(CXXFLAGS) $^ -o $@

clean:
	rm -vrf *.o *.exe *.gch *.dSYM *.stackdump *.out
