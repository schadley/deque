#include "Deque.h"
#include <cassert>
#include <algorithm>

Deque::Deque() : front_start(new int[1]),
    front_first(front_start), front_sz(0), front_cp(1),
    back_start(new int[1]), back_first(back_start),
    back_sz(0), back_cp(1) {}


void Deque::destruct()
{
    delete[] front_start;
    delete[] back_start;
    front_start = nullptr;
    front_first = nullptr;
    back_start = nullptr;
    back_first = nullptr;
}

void Deque::copy(const Deque &other)
{
    front_cp = other.front_cp;
    front_start = new int[front_cp];
    front_first = front_start;
    std::copy(other.front_first, other.front_first + other.front_sz,
        front_first);
    front_sz = other.front_sz;
    back_cp = other.back_cp;
    back_start = new int[back_cp];
    back_first = back_start;
    std::copy(other.back_first, other.back_first + other.back_sz,
        back_first);
    back_sz = other.back_sz;
}

Deque::~Deque()
{
    destruct();
}

Deque & Deque::operator=(const Deque &other)
{
    if (this != &other) {
        destruct();
        copy(other);
    }
    return *this;
}

Deque::Deque(const Deque &other) : front_start(nullptr),
    front_first(nullptr), front_sz(0), front_cp(0),
    back_start(nullptr), back_first(nullptr),
    back_sz(0), back_cp(0)
{
    copy(other);
}

bool Deque::empty()
{
    return (size() == 0);
}

std::size_t Deque::size()
{
    return front_sz + back_sz;
}

int Deque::front()
{
    // The front of an empty deque is invalid
    assert(!empty());
    // If there are elements in the front array,
    // return the front element normally
    if (front_sz > 0)
    {
        return front_first[front_sz - 1];
    }
    // Otherwise return the front of the back array
    else
    {
        return *back_first;
    }
}

int Deque::back()
{
    // The back of an empty deque is invalid
    assert(!empty());
    // If there are elements in the back array,
    // return the back element normally
    if (back_sz > 0)
    {
        return back_first[back_sz - 1];
    }
    // Otherwise return the back of the front array
    else
    {
        return *front_first;
    }
}

void Deque::push_front(int val)
{
    // If the front array is empty, and the back array
    // has an offset, fill in the offset
    if (front_sz == 0 && back_first != back_start)
    {
        --back_first;
        *back_first = val;
        ++back_sz;
    }
    // Otherwise, insert normally into the front array
    else
    {
        // Resize the array if necessary
        if (front_first + front_sz == front_start + front_cp)
        {
            front_cp *= 2;
            int *temp = new int[front_cp];
            std::copy(front_first, front_first + front_sz, temp);
            delete[] front_start;
            front_start = temp;
            front_first = front_start;
        }
        front_first[front_sz] = val;
        ++front_sz;
    }
}

void Deque::push_back(int val)
{
    // If the back array is empty, and the front array
    // has an offset, fill in the offset
    if (back_sz == 0 && front_first != front_start)
    {
        --front_first;
        *front_first = val;
        ++front_sz;
    }
    // Otherwise, insert normally into the back array
    else
    {
        // Resize the array if necessary
        if (back_first + back_sz == back_start + back_cp)
        {
            back_cp *= 2;
            int *temp = new int[back_cp];
            std::copy(back_first, back_first + back_sz, temp);
            delete[] back_start;
            back_start = temp;
            back_first = back_start;
        }
        back_first[back_sz] = val;
        ++back_sz;
    }
}

void Deque::pop_front()
{
    // Popping from an empty deque is invalid
    assert(!empty());
    // If the front array is not empty, pop normally
    if (front_sz > 0)
    {
        --front_sz;
        // If front_sz is empty, reset the offset
        if (front_sz == 0) {
            front_first = front_start;
        }
    }
    // Otherwise, increment the offset to ignore
    // the front element of the back array
    else
    {
        ++back_first;
        --back_sz;
        // If all of the back array is being
        // ignored, clear the array and its offset
        if (back_sz == 0)
        {
            back_first = back_start;
        }
    }
}

void Deque::pop_back()
{
    // Popping from an empty deque is invalid
    assert(!empty());
    // If the back array is not empty, pop normally
    if (back_sz > 0)
    {
        --back_sz;
        // If front_sz is empty, reset the offset
        if (back_sz == 0) {
            back_first = back_start;
        }
    }
    // Otherwise, increment the offset to ignore
    // the front element of the front array
    else
    {
        ++front_first;
        --front_sz;
        // If all of the front array is being
        // ignored, clear the array and its offset
        if (front_sz == 0)
        {
            front_first = front_start;
        }
    }
}

int & Deque::operator[](int index)
{
    if (index < front_sz)
    {
        return front_first[front_sz - index - 1];
    }
    else
    {
        return back_first[index - front_sz];
    }
}
