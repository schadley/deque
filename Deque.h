#ifndef DEQUE_H
#define DEQUE_H

#include <vector>

class Deque
{
private:
    int *front_start, *front_first;
    int front_sz, front_cp;
    int *back_start, *back_first;
    int back_sz, back_cp;

    void destruct();

    void copy(const Deque &other);

public:
    Deque();

    ~Deque();

    Deque & operator=(const Deque &other);

    Deque(const Deque &other);
    
    bool empty();

    std::size_t size();

    int front();

    int back();

    void push_front(int val);

    void push_back(int val);

    void pop_front();

    void pop_back();

    int & operator[](int index);
};

#endif
