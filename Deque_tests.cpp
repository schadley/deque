#include "Deque.h"
#include <cassert>

int main()
{
    Deque test;

    // {}{}
    assert(test.empty());
    assert(test.size() == 0);

    test.push_front(5);
    // {5}{}
    assert(!test.empty());
    assert(test.size() == 1);
    assert(test.front() == 5);
    assert(test.back() == 5);
    assert(test[0] == 5);

    test.push_front(3);
    test.push_back(1);
    // {3,5}{1}
    assert(test.size() == 3);
    assert(test.front() == 3);
    assert(test.back() == 1);
    assert(test[0] == 3);
    assert(test[1] == 5);
    assert(test[2] == 1);

    test.pop_back();
    test.pop_back();
    // {3,X}{}
    assert(!test.empty());
    assert(test.size() == 1);
    assert(test.front() == 3);
    assert(test.back() == 3);
    assert(test[0] == 3);

    test.push_back(6);
    test.push_back(7);
    test.push_back(8);
    test.push_back(9);
    // {3,6}{7,8,9}
    assert(test.size() == 5);
    assert(test.front() == 3);
    assert(test.back() == 9);
    assert(test[0] = 3);
    assert(test[1] == 6);
    assert(test[4] == 9);

    test.pop_front();
    test.pop_front();
    test.pop_front();
    // {}{X,8,9}
    assert(test.size() == 2);
    assert(test.front() == 8);
    assert(test.back() == 9);
    assert(test[0] == 8);
    assert(test[1] == 9);

    test.push_front(1);
    test.push_front(2);
    test.push_front(3);
    // {3,2}{1,8,9}
    test[1] = 10;
    test[3] = 11;
    // {3,10}{1,11,9}
    test.pop_front();
    test.pop_back();
    // {10}{1,11}
    assert(test.size() == 3);
    assert(test.front() == 10);
    assert(test.back() == 11);

    Deque test2(test);
    // test2: {10}{1,11}
    assert(!test2.empty());
    assert(test2.size() == 3);
    assert(test2.front() == 10);
    assert(test2.back() == 11);
    assert(test2[0] == 10);
    assert(test2[1] == 1);
    assert(test2[2] == 11);
    test2.pop_front();
    test2.pop_back();
    // test2: {}{1}
    // test: {10}{1,11}
    assert(test2.size() == 1);
    assert(test.size() == 3);
    assert(test.front() == 10);
    assert(test.back() == 11);

    Deque test3;
    test3.push_front(3);
    test3.push_back(2);
    // test3: {3}{2}
    assert(test3.size() == 2);
    test3 = test;
    // test3: {10}{1,11}
    assert(!test3.empty());
    assert(test3.size() == 3);
    assert(test3.front() == 10);
    assert(test3.back() == 11);
    assert(test3[0] == 10);
    assert(test3[1] == 1);
    assert(test3[2] == 11);
    test3.pop_front();
    test3.pop_back();
    // test3: {}{1}
    // test: {10}{1,11}
    assert(test3.size() == 1);
    assert(test.size() == 3);
    assert(test.front() == 10);
    assert(test.back() == 11);


    return 0;
}